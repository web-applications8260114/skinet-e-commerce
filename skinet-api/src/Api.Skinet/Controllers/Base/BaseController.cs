using Microsoft.AspNetCore.Mvc;

namespace Api.Skinet.Controllers.Base;

[ApiController]
[Route("api/[controller]")]
public class BaseController : ControllerBase
{

}
