using Domain.Skinet.Entities.Base;

namespace Domain.Skinet.Entities;

public class ProductBrand : Entity
{
    public string Name { get; set; }
}
