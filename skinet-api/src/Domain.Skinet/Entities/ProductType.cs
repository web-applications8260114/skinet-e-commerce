using Domain.Skinet.Entities.Base;

namespace Domain.Skinet.Entities;

public class ProductType : Entity
{
    public string Name { get; set; }
}
